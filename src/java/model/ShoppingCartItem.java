/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Etienne
 */

public class ShoppingCartItem implements java.io.Serializable {
    private int id;
    private String name;
    private int quantity;
    private double sellPrice;

    public ShoppingCartItem(int id, String name, int quantity, double sellPrice) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.sellPrice = sellPrice;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(double sellPrice) {
        this.sellPrice = sellPrice;
    }
 
}
