/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Etienne
 */
public class Product implements java.io.Serializable {
    private int id;
    private String name;
    private double sellPrice;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDellPrice(double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getSellPrice() {
        return sellPrice;
    }
    
    public Product(int id, String name, double sellPrice) {
        this.id= id;
        this.name = name;
        this.sellPrice = sellPrice;
    }
}
