/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import model.Product;

/**
 *
 * @author Etienne
 */

@ManagedBean
@ApplicationScoped
public class CatalogManager implements java.io.Serializable {
    private ArrayList<Product> products;
    
    private int newId;
    private String newName;
    private double newSellPrice;

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public double getNewSellPrice() {
        return newSellPrice;
    }

    public void setNewSellPrice(double newSellPrice) {
        this.newSellPrice = newSellPrice;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public CatalogManager() {
        this.products = new ArrayList();
    }
    
    @PostConstruct
    public void initCatalog() {
        this.products.add(new Product(1, "T-shirt", 19.90));
    }
    
    public String createProduct() {
        Product product = new Product(newId, newName, newSellPrice);
        this.products.add(product);
        return "tocatalog";
    }

}

