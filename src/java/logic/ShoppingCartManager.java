/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.ShoppingCartItem;
import model.Product;


/**
 *
 * @author Etienne
 */

@ManagedBean
@SessionScoped
public class ShoppingCartManager {
    private ArrayList<ShoppingCartItem> items;
    private Product prodToAdd;
    private ShoppingCartItem itemToRemove;

    public ShoppingCartManager() {
        this.items = new ArrayList();
    }
    
    public Product getProdToAdd() {
        return prodToAdd;
    }

    public void setProdToAdd(Product prodToAdd) {
        this.prodToAdd = prodToAdd;
    }
    
    public void setItemToRemove(ShoppingCartItem item) {
        this.itemToRemove = item;
    }
    
    public ShoppingCartItem setItemToRemove() {
        return this.itemToRemove;
    }
    
    public ArrayList<ShoppingCartItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<ShoppingCartItem> items) {
        this.items = items;
    }

    public String addToCart() {
        ShoppingCartItem item = new ShoppingCartItem(prodToAdd.getId(), prodToAdd.getName(), 1, prodToAdd.getSellPrice());
        this.items.add(item);
        return "tocart";
    }
    
    @PostConstruct
    public void removeProduct() {
        this.items.remove(this.itemToRemove);
    }
}



